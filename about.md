---
layout: page
title: Sobre
permalink: /about/
---
<main>
<article>
    <h2 class="post-title p-20">{{ page.title | escape }}</h2>
</article>
<!-- conteudo -->
<article class="p-20">

<p><b>{{ site.description | escape }}</b></p>
<p>
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
</p>

<p>
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
</p>

<br><hr><br>

<div class="link-btn">
	<a href="/contact"><h2>Contato</h2></a>
</div>

</article>


<article class="post-card" style="height: 200px; margin-top: 5rem; margin-bottom: 8rem;">
    <a class="post-thumbnail" style="background-color: #3E3E3E height: 200px">
    	<img class="jekylltux" src="{{ site.url }}/img/jekylltux.png" alt="Jekyll e Tux" style="width: 100%;">
    </a>
    <div class="post-item ">
    	<p class="p-10"><h3>Este site foi gerado usando o
            <a href="https://jekyllrb.com/">Jekyll</a>
    		uma ferramenta para criação de sites estáticos escrito em linguagem <a href="https://www.ruby-lang.org/pt/">Ruby</a>
            .<br><br>Hospedagem no Gitlab Pages.
    	</h3></p> 	
    </div>
    <img class="seta" src="/img/footer-arrow.png" style="display: block; float: right;
    padding-top: 15%; padding-right: 5%;">    
</article>
<!-- end conteudo -->
</main>

<div class="footer" style="width: 100%;">
	<h2 style="margin: auto; max-width: 900px; text-align: right;">
     <a href="https://about.gitlab.com/features/pages/">
        <i class="fa fa-gitlab" aria-hidden="true"></i>
        GitLab
     </a>   
    </h2>
</div>
