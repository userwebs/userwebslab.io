---
layout: page
title: Contato
permalink: /contact/
---
<main>
<article>
    <h2 class="post-title p-20">{{ page.title | escape }}</h2>
</article>
<!-- conteudo -->
<style>
.contact-box {
    max-width: 500px;
    margin: auto;
}
.contact-box .card {
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    max-width: 300px;
    margin: auto;
}

.contact-box .card:hover {
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}

.contact-box .card-txt {
    padding: 2px 16px;
}
.contact-box ul {
    text-align: center;
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333333;
}
.contact-box li {
    /*float: left;*/
}
.contact-box li a {
    display: block;
    color: white;
    text-align: center;
    padding: 16px;
    text-decoration: none;
}
.contact-box li a:hover {
    background-color: #111111;
}
</style>

<div class="contact-box">

    {%- if site.author -%}

    <div class="card">
        <img src="/img/img_avatar.png" alt="Avatar" style="width:100%">
         <div class="card-txt">
            <h4><b>{{ site.author | escape }}</b></h4>
            <p>{{ site.aboutme }}</p>
        </div>
    </div>
    <br><hr>

    {%- else -%}
        <p>{{ site.title | escape }}</p>
    {%- endif -%}

<ul>
    {%- if site.email -%}
		<li><a class="u-email" href="mailto:{{ site.email | encode_email }}">
        <i class="fa fa-2x fa-envelope" aria-hidden="true"></i> 
        </a></li>
	{%- endif -%}

    {%- if site.gitlab -%}
        <li><a class="u-gitlab" href="https://gitlab.com/{{ site.gitlab }}">
        <i class="fa fa-2x fa-gitlab" aria-hidden="true"></i>
        </a></li>
    {%- endif -%}
</ul>
</div>
<!-- end conteudo -->
</main>